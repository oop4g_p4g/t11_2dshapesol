
struct AppData
{
	float3 position : POSITION;
    float4 color: COLOR;
};

struct VSOut
{
	float4 color : COLOR;
    float4 position : SV_POSITION;
};

struct AppData2
{
	float3 position : POSITION;
};

struct VSOut2
{
	float4 position : SV_POSITION;
};

