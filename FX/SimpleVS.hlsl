#include "helper.hlsl"

VSOut main( AppData IN )
{
    VSOut OUT;

	OUT.position = float4(IN.position, 1.f);
	OUT.color = IN.color;

    return OUT;
}

