#include "helper.hlsl"

VSOut2 main(AppData2 IN)
{
	VSOut2 OUT;

	OUT.position = float4(IN.position, 1.f);

	return OUT;
}
