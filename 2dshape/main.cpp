#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <vector>

#include "WindowUtils.h"
#include "D3D.h"
#include "Sprite.h"
#include "ShaderTypes.h"
#include "FX.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;


ID3D11InputLayout* gInputLayout, *gInputLayoutFixed;
ID3D11Buffer* gBoxVB;
ID3D11Buffer* gBoxIB;
ID3D11VertexShader* pVertexShader = nullptr;
ID3D11PixelShader* pPixelShader = nullptr;


ID3D11VertexShader* pVSFixed = nullptr;
ID3D11PixelShader* pPSFixed = nullptr;
ID3D11Buffer* gTankVB, *gTankIB;
ID3D11Buffer* gCrossVB, *gCrossIB;

void Translate(VertexPos verts[], int numV, const Vector3& offset)
{
	for (int i = 0; i < numV; ++i)
		verts[i].Pos += offset;
}

void Scale(VertexPos verts[], int numV, const Vector3& scale)
{
	for (int i = 0; i < numV; ++i)
		verts[i].Pos *= scale;
}

void Translate(VertexPosColour verts[], int numV, const Vector3& offset)
{
	for (int i = 0; i < numV; ++i)
		verts[i].Pos += offset;
}

void Scale(VertexPosColour verts[], int numV, const Vector3& scale)
{
	for (int i = 0; i < numV; ++i)
		verts[i].Pos *= scale;
}


void BuildGeometryBuffers()
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	// Create vertex buffer for a quad (two triangle square)
	VertexPosColour vertices[] =
	{
		{ Vector3(-0.5f, -0.5f, 0.f), Colors::White   },
		{ Vector3(-0.5f, +0.5f, 0.f), Colors::Black },
		{ Vector3(+0.5f, +0.5f, 0.f), Colors::Red },
		{ Vector3(+0.5f, -0.5f, 0.f), Colors::Green }
	};
	Scale(vertices, 4, Vector3(0.5f, 0.5f, 0.5f));
	Translate(vertices, 4, Vector3(-0.5, 0, 0));
	CreateVertexBuffer(d3d.GetDevice(),sizeof(VertexPosColour) * 4, vertices, gBoxVB);


	// Create the index buffer

	UINT indices[] = {
		// front face
		0, 1, 2,
		0, 2, 3
	};

	CreateIndexBuffer(d3d.GetDevice(), sizeof(UINT) * 6, indices, gBoxIB);

	//cross
	VertexPosColour vertices3[] =
	{
		{ Vector3(.2f, 0.f, 0.f), Colors::Green },
		{ Vector3(0.f, 0.2f, 0.f), Colors::Green },
		{ Vector3(0.4f,.5f, 0.f), Colors::Green },
		{ Vector3(0.f, .8f, 0.f), Colors::Green },
		{ Vector3(.2f, 1.f, 0.f), Colors::Green },
		{ Vector3(.5f, .6f, 0.f), Colors::Green },
		{ Vector3(.8f, 1.f, 0.f), Colors::Green },
		{ Vector3(1.f, .8f, 0.f), Colors::Green },
		{ Vector3(.6f, .5f, 0.f), Colors::Green },
		{ Vector3(1.f, .2f, 0.f), Colors::Green },
		{ Vector3(.8f, 0.f, 0.f), Colors::Green },
		{ Vector3(.5f, .4f, 0.f), Colors::Green }
	};
	Scale(vertices3, 12, Vector3(0.75f, 0.75f, 0.75f));
	Translate(vertices3, 12, Vector3(0, -0.75, 0));
	CreateVertexBuffer(d3d.GetDevice(), sizeof(VertexPosColour) * 12, vertices3, gCrossVB);
	UINT indices3[] = {
		0,1,2,
		0,2,11,
		2,3,4,
		2,4,5,
		11,2,5,
		11,5,8,
		8,5,6,
		8,6,7,
		10,11,8,
		10,8,9
	};
	CreateIndexBuffer(d3d.GetDevice(), sizeof(UINT) * 30, indices3, gCrossIB);

	//tank
	VertexPos vertices2[] =
	{
		{ Vector3(0.f, 0.f, 0.f) },
		{ Vector3(.6f, 0.f, 0.f) },
		{ Vector3(0.f, .3f, 0.f) },
		{ Vector3(.3f, .3f, 0.f) },
		{ Vector3(.6f, .3f, 0.f) },
		{ Vector3(.9f, .3f, 0.f) },
		{ Vector3(0.f, .6f, 0.f) },
		{ Vector3(.3f, .6f, 0.f) },
		{ Vector3(.6f, .6f, 0.f) },
		{ Vector3(.9f, .6f, 0.f) },
		{ Vector3(0.f, .9f, 0.f) },
		{ Vector3(.6f, .9f, 0.f) }
	};
	Scale(vertices2, 12, Vector3(0.5f, 0.5f, 0.5f));
	Translate(vertices2, 12, Vector3(0, 0.25, 0));
	CreateVertexBuffer(d3d.GetDevice(), sizeof(VertexPos) * 12, vertices2, gTankVB);
	UINT indices2[] = {
		0,2,3,
		0,3,1,
		1,3,4,
		3,7,8,
		3,8,4,
		4,8,9,
		4,9,5,
		6,10,7,
		7,10,11,
		7,11,8
	};
	CreateIndexBuffer(d3d.GetDevice(), sizeof(UINT) * 30, indices2, gTankIB);


}



bool BuildFX()
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	CheckShaderModel5Supported(d3d.GetDevice());

	//load in a pre-compiled vertex shader
	char* pBuff = nullptr;
	unsigned int bytes = 0;
	pBuff = ReadAndAllocate("../bin/data/SimpleVS.cso", bytes);
	CreateVertexShader(d3d.GetDevice(),pBuff, bytes, pVertexShader);
	//create a link between our data and the vertex shader
	CreateInputLayout(d3d.GetDevice(),VertexPosColour::sVertexDesc, 2, pBuff, bytes, &gInputLayout);
	delete[] pBuff;

	//load in a pre-compiled pixel shader	
	pBuff = ReadAndAllocate("../bin/data/SimplePS.cso", bytes);
	CreatePixelShader(d3d.GetDevice(),pBuff, bytes, pPixelShader);
	delete[] pBuff;


	pBuff = ReadAndAllocate("../bin/data/FixedVS.cso", bytes);
	CreateVertexShader(d3d.GetDevice(),pBuff, bytes, pVSFixed);
	CreateInputLayout(d3d.GetDevice(),VertexPos::sVertexDesc, 1, pBuff, bytes, &gInputLayoutFixed);
	delete[] pBuff;


	pBuff = ReadAndAllocate("../bin/data/FixedPS.cso", bytes);
	CreatePixelShader(d3d.GetDevice(),pBuff, bytes, pPSFixed);
	delete[] pBuff;


	return true;

}



void InitGame()
{
	BuildGeometryBuffers();
	BuildFX();
}

void ReleaseGame()
{
	ReleaseCOM(pVertexShader);
	ReleaseCOM(pPixelShader);
	ReleaseCOM(pVSFixed);
	ReleaseCOM(pPSFixed);
	ReleaseCOM(gBoxVB);
	ReleaseCOM(gBoxIB);
	ReleaseCOM(gCrossVB);
	ReleaseCOM(gCrossIB);
	ReleaseCOM(gTankVB);
	ReleaseCOM(gTankIB);
	ReleaseCOM(gInputLayout);
	ReleaseCOM(gInputLayoutFixed);
}

static float gAngle = 0;


void Update(float dTime)
{
	gAngle += dTime * 0.5f;
}

void Render()
{
	MyD3D& d3d = WinUtil::Get().GetD3D();
	d3d.BeginRender(Vector4(0, 0, 0, 1));

	d3d.InitInputAssembler(gInputLayout, gBoxVB, sizeof(VertexPosColour), gBoxIB);
	d3d.GetDeviceCtx().VSSetShader(pVertexShader, nullptr, 0);
	d3d.GetDeviceCtx().PSSetShader(pPixelShader, nullptr, 0);
	d3d.GetDeviceCtx().DrawIndexed(6, 0, 0);

	d3d.InitInputAssembler(gInputLayout, gCrossVB, sizeof(VertexPosColour), gCrossIB);
	d3d.GetDeviceCtx().DrawIndexed(30, 0, 0);

	d3d.GetDeviceCtx().VSSetShader(pVSFixed, nullptr, 0);
	d3d.GetDeviceCtx().PSSetShader(pPSFixed, nullptr, 0);
	d3d.InitInputAssembler(gInputLayoutFixed, gTankVB, sizeof(VertexPos), gTankIB);
	d3d.GetDeviceCtx().DrawIndexed(30, 0, 0);

	d3d.EndRender();
}


//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	d3d.OnResize_Default(screenWidth, screenHeight);
}

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			break;
		}
	case WM_INPUT:
		break;
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(512), h(256);
	//int defaults[] = { 640,480, 800,600, 1024,768, 1280,1024 };
		//WinUtil::ChooseRes(w, h, defaults, 4);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Fezzy", MainWndProc, true))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);
	d3d.GetCache().SetAssetPath("data/");

	InitGame();

	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender && dTime>0)
		{
			Update(dTime);
			Render();
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
	}

	d3d.ReleaseD3D(true);	
	return 0;
}

